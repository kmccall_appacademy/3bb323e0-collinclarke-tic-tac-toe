
require 'byebug'

class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark


  def initialize(name)
    @name = name
    @mark = :O
  end


  def display(board)
    @board = board
  end

  def get_move
    moves = []
    board.grid.each_with_index do |row, y|
     row.each_with_index do |element, x|
      if board.empty?([x, y])
       if wins?([x, y], mark)
        return [x, y]
       else
        moves << [x, y]
       end
      end
     end
    end
   moves.sample
  end

  def wins?(pos, mark)
    board.place_mark(pos, mark)
     if board.winner == mark
       board.place_mark(pos, nil)
       return true
     else
       board.place_mark(pos, nil)
       return false
     end
  end

end
