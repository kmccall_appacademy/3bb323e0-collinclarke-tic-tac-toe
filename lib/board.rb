class Board

  attr_reader :grid, :marks

  def self.blank_grid
    Array.new(3) { Array.new(3) }
  end

  def initialize(grid = Board.blank_grid)
    @grid = grid
    @marks = [:X, :O]
  end

  def place_mark(pos, mark)
    grid[pos.first][pos.last] = mark
  end

  def empty?(pos)
    grid[pos.first][pos.last] == nil
  end


  def winner
    if row_winner
      return row_winner[0]
    elsif column_winner
      return column_winner[0]
    elsif diagonal_winner
      return diagonal_winner[0]
    else
      nil
    end
  end

  def over?
    if winner
      return true
    elsif grid.all?{|rows| rows.all?{|el| el}}
      return true
    else
      false
    end
  end



  def one
    grid[0][0]
  end
  def two
    grid[0][1]
  end
  def three
    grid[0][2]
  end
  def four
    grid[1][0]
  end
  def five
    grid[1][1]
  end
  def six
    grid[1][2]
  end
  def seven
    grid[2][0]
  end
  def eight
    grid[2][1]
  end
  def nine
    grid[2][2]
  end


  def lines
    [diagonals, columns, grid]
  end

  def left_diagonal
    [one, five, nine]
  end

  def right_diagonal
    [three, five, seven]
  end

  def diagonals
    [left_diagonal, right_diagonal]
  end

  def columns
    [left_column, mid_column, right_column]
  end

  def left_column
    [one, four, seven]
  end

  def mid_column
    [two, five, eight]
  end

  def right_column
    [three, six, nine]
  end

  def top_row
    grid[0]
  end

  def mid_row
    grid[1]
  end

  def bot_row
    grid[2]
  end


  def win(group)
    group.select{|line| line.all?{|el| el && line.uniq.length == 1}}
  end

  def row_winner
    win(grid)[0]
  end

  def column_winner
    win(columns)[0]
  end

  def diagonal_winner
   win(diagonals)[0]
  end

end
