require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require 'byebug'

class Game
  attr_accessor :current_player, :player_one, :player_two, :board
  def initialize(player_one, player_two)
    @player_one, @player_two = player_one, player_two
    @player_one.mark = :X
    @player_two.mark = :O
    @current_player = @player_one
    @board = Board.new
  end

  def play
    # debugger
    until board.over?
      play_turn
    end
    result(board)
  end

  def result(board)
    unless board.winner
      puts "cats game!"
    end
    if board.winner == :O
      puts "second player wins!"
    else
      puts "first player wins!"
    end
    player_one.display(board)
    puts "play again? (y/n)"
    input = gets.chomp
    if input == 'y'
      Game.new(player_one, player_two).play
    end
  end


  def play_turn
    current_player.display(board)
    move = current_player.get_move
    until board.empty?(move)
      if current_player.class == HumanPlayer
      puts "sorry that space is occupied, select a new position"
      end
      move = current_player.get_move
    end
    board.place_mark(move, current_player.mark)
    switch_players!
  end

  def switch_players!
   @current_player = current_player == player_one ? player_two : player_one
  end

end

if $PROGRAM_NAME == __FILE__
  puts "Hello, welcome to tic tac toe, please enter your name"
  player_1 = gets.chomp
  human = HumanPlayer.new(player_1)
  puts "Will you be playing against me or some other human?"
  input = gets.chomp
  if input.downcase != 'you'
    puts "What is the second players name?"
    player_2 = gets.chomp
    human_2 = HumanPlayer.new(player_2)
    Game.new(human, human_2).play
  else
    coraline = ComputerPlayer.new('coraline')
    puts "*sigh* ok.. but you know this game is pointless"
    Game.new(human, coraline).play
  end
 end
